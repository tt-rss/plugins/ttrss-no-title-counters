<?php
class No_Title_Counters extends Plugin {

	function about() {
		return array(null,
			"Remove counters from window title (prevents tab flashing on new articles)",
			"fox");
	}

	function init($host) {

	}

	function get_js() {
		return file_get_contents(__DIR__ . "/init.js");
	}

	function api_version() {
		return 2;
	}

}
